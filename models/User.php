<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property integer $confirmEmail
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['login', 'password', 'email'], 'required'],
            [['confirmEmail'], 'integer'],
            [['login'], 'unique'],
            [['email'], 'unique'],
            [['login', 'password', 'email'], 'string', 'max' => 255],
            ['confirmEmail', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'email' => 'Email',
            'confirmEmail' => 'Подтверждение электронной почты',
        ];
    }

    static function encryptPassword($password) {
        return md5($password);
    }

    public function getAuthKey() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

    public function validatePassword($password) {
        $pass = self::encryptPassword($password);
        if ($pass == $this->password) {
            return true;
        } else {
            return false;
        }
    }

    function sendEmailNotification($subject, $message) {
        Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($this->email)
                ->setSubject($subject)
                ->setTextBody($message)
                ->send();
    }

    function setFlash($title, $message) {
        Yii::$app->session->setFlash('suc', "");
    }

    public function reg() {
        $this->password = self::encryptPassword($this->password);
        $this->save();
    }

    function notification() {
        $this->sendEmailNotification('текст', 'text');
    }

}
