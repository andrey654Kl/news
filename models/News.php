<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $preview
 * @property string $text
 */
class News extends \yii\db\ActiveRecord {

    static $countNewsPerPage = [3, 5, 10, 15];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'preview', 'text'], 'required'],
            [['preview', 'text'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'preview' => 'Превью',
            'text' => 'Текст',
        ];
    }

    function notificationUsers() {
        $users = User::find()->all();
        foreach ((array) $users as $user) {
            $user->notification();
        }
    }

}
