<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Event;

/**
 * Description of BaseController
 *
 * @author kas
 */
class BaseController extends Controller {

    //put your code here
    public function init() {
        parent::init();
    }

}
