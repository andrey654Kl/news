<?php
/* @var $this yii\web\View */
$this->title = 'Новости';
use yii\widgets\Pjax;
use yii\helpers\Html;
if ($msg = Yii::$app->session->getFlash('suc')) { echo $msg; } 
$isGuest = Yii::$app->user->isGuest;
$pageSize = $provider->getPagination()->pageSize;
?>
<div class="site-index">
    <h1>Список новостей</h1>
    <div id="news_container" class="<?= (!$isGuest ? 'for_user' : '') ?>">
        <?php Pjax::begin(); ?>
        <?php echo $this->render('_newsContainer', ['provider' => $provider]) ?>
        <div id="choose_count">Выберите количество отображаемых новостей на странице</div>
        <?php
        foreach ((array) app\models\News::$countNewsPerPage as $button) {
            echo Html::a($button, ['site/index', 'count' => $button], ['class' => "btn-default btn-sm " . ($button == $pageSize ? 'active' : 0)]);
        }
        ?>
<?php Pjax::end(); ?>
    </div>
</div>