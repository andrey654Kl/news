<?php
use yii\widgets\ListView;

echo ListView::widget([
    'dataProvider' => $provider,
    'itemView' => '_news',
     'layout' => "{items}\n{pager}",
]);
?>