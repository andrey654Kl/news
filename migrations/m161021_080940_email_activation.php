<?php

use yii\db\Migration;

class m161021_080940_email_activation extends Migration
{
    public function up()
    {
          $this->createTable('user_email_activation', [
              'userID'=>$this->primaryKey(),
              'code'=>$this->string()->notNull(),
              'activateTime'=>$this->dateTime()
          ]);
    }

    public function down()
    {
       $this->dropTable('user_email_activation');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
