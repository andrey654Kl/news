<?php

use yii\db\Migration;

class m161019_055002_add_news extends Migration
{
    public function up()
    {
       $this->createTable('news', [
           'id'=>$this->primaryKey(),
           'title'=>$this->string(255)->notNull(),
           'preview'=>$this->text()->notNull(),
           'text'=>$this->text()->notNull(),
       ]);
    }

    public function down()
    {
        $this->dropTable('news');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
