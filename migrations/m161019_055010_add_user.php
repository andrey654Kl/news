<?php

use yii\db\Migration;
use app\models\User;

class m161019_055010_add_user extends Migration {

    public function up() {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string(255)->notNull(),
            'password' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'confirmEmail' => $this->boolean(),
        ]);
        $this->insert('user', ['id'=>1, 'login'=>'user', 'password' => User::encryptPassword('user'), 'email' =>'user@user.ru', 'confirmEmail' =>true]);
        $this->insert('user', ['id'=>2, 'login'=>'moder', 'password' => User::encryptPassword('moder'), 'email' =>'moder@moder.ru', 'confirmEmail' =>true]);
        $this->insert('user', ['id'=>3, 'login'=>'admin', 'password' => User::encryptPassword('admin'), 'email' =>'admin@admin.ru', 'confirmEmail' =>true]);
    }

    public function down() {
        $this->dropTable('user');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
